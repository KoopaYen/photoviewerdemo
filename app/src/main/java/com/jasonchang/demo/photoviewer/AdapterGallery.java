package com.jasonchang.demo.photoviewer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jasonchang.demo.photoviewer.model.gson.Data;
import com.jasonchang.demo.photoviewer.model.gson.Image;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jason.chang on 16/1/7.
 */
public class AdapterGallery extends RecyclerView.Adapter<AdapterGallery.ViewHolder> {

    private Context context;

    private List<Data> dataList;

    private AdapterGalleryCallback callback;

    public AdapterGallery(Context context) {
        this.dataList = new ArrayList<>();
        this.context = context;
        this.callback = (AdapterGalleryCallback) context;
    }

    public void updateDataList(List<Data> list) {
        this.dataList.clear();
        this.dataList.addAll(list);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (this.dataList != null) {
            return this.dataList.size();
        }
        return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View convertView = inflater.inflate(R.layout.list_item_gallery, parent, false);

        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // take third large image.
        Image image = dataList.get(position).getImages().get(2);

        holder.setPosition(position);

        Picasso.with(context)
                .load(image.getSource())
                .placeholder(R.drawable.com_facebook_button_icon)
                .into(holder.photo);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.imageView_photo)
        ImageView photo;

        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(ViewHolder.this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(position);
                }
            });
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    public interface AdapterGalleryCallback {
        void onItemClick(int position);
    }
}
