package com.jasonchang.demo.photoviewer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jasonchang.demo.photoviewer.model.gson.Data;
import com.jasonchang.demo.photoviewer.model.gson.Pictures;
import com.jasonchang.demo.photoviewer.model.gson.SugarData;
import com.jasonchang.demo.photoviewer.widget.HackyViewPager;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoView;

/**
 * Created by jason.chang on 16/1/7.
 */
public class ActivityPhotoViewer extends Activity {

    @Bind(R.id.view_pager)
    HackyViewPager viewPager;

    private List<Data> dataList;

    private SugarData sugarData;

    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        ButterKnife.bind(ActivityPhotoViewer.this);

        sugarData = SugarData.findById(SugarData.class, 1);
        if (sugarData != null) {
            if (getIntent() != null && getIntent().getExtras() != null) {
                currentPosition = getIntent().getExtras().getInt(Constants.PROPERTY_POSITION);
            }

            dataList = new Gson().fromJson(sugarData.getDataString(), Pictures.class).getData();

            if (dataList.size() == 0) {
                finish();
            }

            PhotoPagerAdapter adapter = new PhotoPagerAdapter(ActivityPhotoViewer.this, viewPager, dataList);
            viewPager.setAdapter(adapter);
            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

                @Override
                public void onPageSelected(int position) {}

                @Override
                public void onPageScrollStateChanged(int state) {}
            });
            viewPager.setCurrentItem(currentPosition);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(ActivityPhotoViewer.this);
    }

    class PhotoPagerAdapter extends PagerAdapter {

        Context context;

        List<Data> dataList;

        HackyViewPager hackyViewPager;

        public PhotoPagerAdapter(Context context, HackyViewPager pager, List<Data> dataList) {
            this.context = context;
            this.hackyViewPager = pager;
            this.dataList = dataList;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final PhotoView photoView = new PhotoView(container.getContext());

            Picasso.with(context)
                    .load(dataList.get(position).getImages().get(0).getSource()) // load largest image.
                    .into(photoView);

            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
