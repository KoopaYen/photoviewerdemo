package com.jasonchang.demo.photoviewer;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jason.chang on 16/1/6.
 */
public class ActivityMain extends AppCompatActivity {

    @Bind(R.id.login_button)
    LoginButton loginBtn;

    @Bind(R.id.btn_animation)
    View btnAnimation;

    private CallbackManager callbackManager;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(ActivityMain.this);

        callbackManager = CallbackManager.Factory.create();

        loginBtn.setReadPermissions("user_photos");
        loginBtn.registerCallback(
                callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        animate();
                    }

                    @Override
                    public void onCancel() {}

                    @Override
                    public void onError(FacebookException error) {}
                });

        if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
            startActivity(new Intent(ActivityMain.this, ActivityPhotoGallery.class));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ButterKnife.unbind(ActivityMain.this);
    }

    private void animate() {
        loginBtn.animate().setInterpolator(new AccelerateInterpolator()).scaleX(0f).setDuration(200).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {

                btnAnimation.setVisibility(View.VISIBLE);

                btnAnimation.animate().setInterpolator(new AccelerateInterpolator()).scaleXBy(250).scaleYBy(250).setDuration(1500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
                                    startActivity(new Intent(ActivityMain.this, ActivityPhotoGallery.class));
                                    finish();
                                }
                            }
                        },300);
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {}

                    @Override
                    public void onAnimationCancel(Animator animator) {}

                    @Override
                    public void onAnimationRepeat(Animator animator) {}
                });

            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }
}
