package com.jasonchang.demo.photoviewer.model.gson;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jason.chang on 16/1/6.
 */
@Generated("org.jsonschema2pojo")
public class Data {

    @SerializedName("images")
    @Expose
    private List<Image> images = new ArrayList<Image>();

    @SerializedName("id")
    @Expose
    private String id;

    /**
     *
     * @return
     * The images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     *
     * @param images
     * The images
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }
}