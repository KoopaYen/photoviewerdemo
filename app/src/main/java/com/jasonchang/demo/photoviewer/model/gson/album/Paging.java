package com.jasonchang.demo.photoviewer.model.gson.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by jason.chang on 16/1/12.
 */
@Generated("org.jsonschema2pojo")
public class Paging {

    @SerializedName("cursors")
    @Expose
    private Cursors cursors;

    /**
     *
     * @return
     * The cursors
     */
    public Cursors getCursors() {
        return cursors;
    }

    /**
     *
     * @param cursors
     * The cursors
     */
    public void setCursors(Cursors cursors) {
        this.cursors = cursors;
    }
}
