package com.jasonchang.demo.photoviewer.model.gson;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jason.chang on 16/1/6.
 */
@Generated("org.jsonschema2pojo")
public class Pictures {

    @SerializedName("data")
    @Expose
    private List<Data> data = new ArrayList<Data>();

    /**
     * @return The data
     */
    public List<Data> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Data> data) {
        this.data = data;
    }

}
