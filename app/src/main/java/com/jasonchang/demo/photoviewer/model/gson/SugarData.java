package com.jasonchang.demo.photoviewer.model.gson;

import com.orm.SugarRecord;

/**
 * Created by jason.chang on 16/1/7.
 */
public class SugarData extends SugarRecord {

    private String dataString;

    public String getDataString() {
        return dataString;
    }

    public void setDataString(String dataString) {
        this.dataString = dataString;
    }
}
