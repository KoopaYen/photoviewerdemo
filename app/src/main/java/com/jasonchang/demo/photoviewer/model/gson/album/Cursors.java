package com.jasonchang.demo.photoviewer.model.gson.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by jason.chang on 16/1/12.
 */
@Generated("org.jsonschema2pojo")
public class Cursors {

    @SerializedName("after")
    @Expose
    private String after;
    @SerializedName("before")
    @Expose
    private String before;

    /**
     *
     * @return
     * The after
     */
    public String getAfter() {
        return after;
    }

    /**
     *
     * @param after
     * The after
     */
    public void setAfter(String after) {
        this.after = after;
    }

    /**
     *
     * @return
     * The before
     */
    public String getBefore() {
        return before;
    }

}
