package com.jasonchang.demo.photoviewer;

import com.facebook.FacebookSdk;
import com.orm.SugarApp;

/**
 * Created by jason.chang on 16/1/6.
 */
public class ApplicationMain extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
