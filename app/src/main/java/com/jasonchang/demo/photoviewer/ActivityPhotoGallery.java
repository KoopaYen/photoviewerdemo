package com.jasonchang.demo.photoviewer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;
import com.jasonchang.demo.photoviewer.model.gson.Data;
import com.jasonchang.demo.photoviewer.model.gson.Pictures;
import com.jasonchang.demo.photoviewer.model.gson.SugarData;
import com.jasonchang.demo.photoviewer.model.gson.album.Album;
import com.jasonchang.demo.photoviewer.model.gson.album.AlbumData;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * Created by jason.chang on 16/1/7.
 */
public class ActivityPhotoGallery extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterGallery.AdapterGalleryCallback {

    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.recyclerView_photos)
    RecyclerView recyclerViewPhotos;

    AdapterGallery adapterGallery;

    private SugarData sugarData;

    private Pictures mPictures;

    @Override
    public void onRefresh() {
        mPictures.getData().clear();
        fetchDataFromFacebook();
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(ActivityPhotoGallery.this, ActivityPhotoViewer.class);

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PROPERTY_POSITION, position);

        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPictures = new Pictures();
        mPictures.setData(new ArrayList<Data>());

        setContentView(R.layout.activity_photo_gallery);

        ButterKnife.bind(ActivityPhotoGallery.this);

        init();

        fetchDataFromFacebook();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ButterKnife.unbind(ActivityPhotoGallery.this);
    }

    private void init() {

        sugarData = SugarData.findById(SugarData.class, 1);
        if (sugarData == null) {
            sugarData = new SugarData();
        }

        swipeRefreshLayout.setOnRefreshListener(ActivityPhotoGallery.this);

        adapterGallery = new AdapterGallery(ActivityPhotoGallery.this);
        recyclerViewPhotos.setAdapter(adapterGallery);
        recyclerViewPhotos.setHasFixedSize(true);

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(ActivityPhotoGallery.this, 6);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int index = position % 5;
                switch (index) {
                    case 0:
                        return 2;
                    case 1:
                        return 2;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                    case 4:
                        return 3;
                }
                return 0;
            }
        });

        recyclerViewPhotos.setLayoutManager(gridLayoutManager);
        recyclerViewPhotos.addItemDecoration(new SpacesItemDecoration(10));
        recyclerViewPhotos.setItemAnimator(new SlideInUpAnimator());
    }

    private void fetchDataFromFacebook() {

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(true);
        }

        if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {

            final String userId = AccessToken.getCurrentAccessToken().getUserId();

            /* make the API call */
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/" + userId + "/albums",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            /* handle the result */
                            AlbumData data = new Gson().fromJson(response.getRawResponse(), AlbumData.class);
                            getAllPhotos(data);
                        }
                    }
            ).executeAsync();

        }
    }

    private void getAllPhotos(AlbumData data) {

        Bundle parameters = new Bundle();
        parameters.putString("fields", "images");

        for (Album id : data.getData()) {

            /* make the API call */
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/" + id.getId() + "/photos",
                    parameters,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            /* handle the result */

                            Pictures pictures = new Gson().fromJson(response.getRawResponse(), Pictures.class);

                            mPictures.getData().addAll(pictures.getData());
                            adapterGallery.updateDataList(mPictures.getData());
                            adapterGallery.notifyDataSetChanged();

                            sugarData.setDataString(new Gson().toJson(mPictures, Pictures.class));
                            sugarData.save();

                            if (swipeRefreshLayout != null) {
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    }
            ).executeAsync();
        }
    }

}
